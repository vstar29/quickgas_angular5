import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AccountComponent} from './account/account.component';
import {FrameComponent} from './frame/frame.component';
import {VendorComponent} from './vendor/vendor.component';
import {AreasComponent} from './areas/areas.component';
import {FaqComponent} from './faq/faq.component';
import {LegalComponent} from './legal/legal.component';
import {CareersComponent} from './careers/careers.component';
import {ContactComponent} from './contact/contact.component';



const routes: Routes = [

	 {path: '', component: FrameComponent, children: [
        {path: '', component: HomeComponent}, 
        {path: 'account', component: AccountComponent},
        {path: 'vendors', component: VendorComponent},
        {path: 'areas', component: AreasComponent},
        {path: 'faq', component: FaqComponent},
        {path: 'legal', component: LegalComponent},
        {path: 'contact', component: ContactComponent},
        {path: 'careers', component: CareersComponent}
        
    ]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
