export class User {

	constructor(
		public id? :number,
		public name? :string,
		public email? :string,
		public password?: string,
		public phonenumber? :number,
		public area?:string,
		public term? :boolean,
		public type?:number)
		{}

	
}
