import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {User} from '../user';
import {UserService} from '../user.service';
import {Observable} from 'rxjs/Rx';
import { Router } from '@angular/router';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
	type:string;
  user=new User();
  Areas: Array<string>; 
  main_div:boolean=true;
  e_div:boolean=false;
  p_div:boolean=false;
  s_div:boolean=false;
  phone_t="";
  email_t="";
  r_error="";

  constructor(private userService:UserService,private router:Router) { 
  		
  }

  ngOnInit() {
     this.Areas = ['Ikeja','Abule Egba'];
  }

  onRegister(regForm) {
  this.r_error="";
    if(regForm.valid)
    {
      
      this.userService.makePOST('register',this.user).subscribe(
       data => {
       this.main_div=false;
       this.e_div=true;
       },
       error => {
       error=error.error;
        this.r_error=error;
       if(error=='Expired session')
       {
          this.e_div=false;
          this.s_div=false;
          this.main_div=true;
       }
      
       this.user.password="";
       return Observable.throw(error);
       }
    );

    }

  }

  /*--email verification function--*/
  onEToken(emailForm) {
    this.r_error="";
    if(emailForm.valid)
    {
      let details=
      {
        'e_code':this.email_t
      };
      this.userService.makePOST('verify_email',details).subscribe(
       data => {
       this.e_div=false;
       this.p_div=true;
       },
       error => {
        error=error.error;
        this.r_error=error;
       if(error=='Expired session')
       {
          this.e_div=false;
          this.s_div=false;
          this.main_div=true;
       }
       
        this.email_t="";
        return Observable.throw(error);
       }
    );

    }

  }

/*---phone number verification token--*/
  onPToken(phoneForm) {
    this.r_error="";
    if(phoneForm.valid)
    {
      let details=
      {
        'p_code':this.phone_t
      };
      this.userService.makePOST('adduser',details).subscribe(
       data => {
       this.e_div=false;
       this.s_div=true;
       },
       error => {
       error=error.error;
       this.r_error=error;
       if(error=='Expired session')
       {
          this.e_div=false;
          this.s_div=false;
          this.main_div=true;
       }
       
        this.phone_t="";
        return Observable.throw(error);
       }
    );

    }

  }

}
