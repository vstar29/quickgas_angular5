import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
const baseUrl='http://qg.com/api/web/';

@Injectable()
export class UserService {
  
  constructor(private http:HttpClient) { }

  makePOST(url,data)
  {
  
    return this.http.post(baseUrl+url, data, httpOptions);
  }

}
